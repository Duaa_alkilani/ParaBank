// @ts-check
const { test, expect } = require("@playwright/test");
const { error } = require("console");
// @ts-ignore
import { LoginPage } from "../pages/login";
// @ts-ignore
import { SignUp } from "../pages/signup";
import { Accounts } from "../pages/myAccounts";
import { Transfer } from "../pages/transfer";
import { AccountsOverview } from "../pages/accountsOverview";
import { Loan } from "../pages/loan";
import { BillPayment } from "../pages/billPaymentService";
const exp = require("constants");
test.describe("para bank website", () => {
  test("test going to ParaBank page ", async ({ page },testInfo) => {
    await page.goto("http://localhost:8080/parabank/index.htm?ConnType=JDBC");

    // Expect a title "to contain" a substring.
    await expect(page).toHaveTitle("ParaBank | Welcome | Online Banking");
    //add screen shot for result 
    await testInfo.attach("home page",{
      body:await page.screenshot(),
      contentType:"image/png"
    })
  });

  test("login with valid cred", async ({ page },testInfo) => {
    await page.goto("http://localhost:8080/parabank/index.htm?ConnType=JDBC");
    await expect(page).toHaveTitle("ParaBank | Welcome | Online Banking");
    await page.locator('input[name="username"]').fill("ali");
    await page.locator('input[name="password"]').fill("akram");
    await page.getByRole("button", { name: "Log In" }).click();
    await expect(page).toHaveTitle("ParaBank | Accounts Overview");
//add screen shot 
    await testInfo.attach("login with valid cred",{
      body:await page.screenshot(),
      contentType:"image/png"
    })

  });

  test("login with invalid cred", async ({ page },testInfo) => {
    await page.goto("http://localhost:8080/parabank/index.htm?ConnType=JDBC");
    await expect(page).toHaveTitle("ParaBank | Welcome | Online Banking");
    await page.locator('input[name="username"]').fill("ali");
    await page.locator('input[name="password"]').fill("akramnn");
    await page.getByRole("button", { name: "Log In" }).click();

    const messageError = page.getByRole("heading", { name: "Error!" });
    await expect(messageError).toBeVisible();
//add screen shot 

    await testInfo.attach("login with invalid cred",{
      body:await page.screenshot(),
      contentType:"image/png"
    })

  });

  test(" test  login fun should redirect to home page ", async ({ page },testInfo) => {
    const Login = new LoginPage(page);
    await Login.goToLoginPage();
    await Login.doLogin({ userName: "ali", password: "akram" });
    await expect(page).toHaveTitle("ParaBank | Accounts Overview");
    //add screen shot 
    await testInfo.attach("login with invalid cred",{
      body:await page.screenshot(),
      contentType:"image/png"
    })
  });

  test(" test signUp  function should created the account  successfully ", async ({
    page,
  },testInfo) => {
    const SignUpObj = new SignUp(page);
    await SignUpObj.doGoToRegsiter();
    //genrate random name for username

    let randomUserName = (Math.random() + 1).toString(36).substring(7);
    console.log("random user name ==", randomUserName);
    await SignUpObj.doSignUp(
      "mo",
      "ahmed",
      "7thsss",
      "cairo",
      "cairo",
      "1233999",
      "122",
      randomUserName,
      "alki",
      "alki"
    );
    // const locator= page.getByRole('heading', { name: 'Welcome' })
    const locator = page.locator(
      "//p[contains(text(),'Your account was created successfully. You are now')]"
    );
    await expect(locator).toHaveText(
      "Your account was created successfully. You are now logged in.")

      //add screen shot 
    await testInfo.attach("test signUp  function",{
      body:await page.screenshot(),
      contentType:"image/png"
    })
   
  });
  test.only('create 3 account has total balance as expected "3,500,000" ', async ({
    page,
  },testInfo) => {
    //create a new object from the classes
    const SignUpObj = new SignUp(page);
    const account = new Accounts(page);
    const AccountOverview = new AccountsOverview(page);
    // go to sign up  page
    //genrate random name for username

    await SignUpObj.doGoToRegsiter();
    let randomUserName = (Math.random() + 1).toString(36).substring(7);
    console.log("random user name ==", randomUserName);
    const user = await SignUpObj.doSignUp(
      "mo",
      "ahmed",
      "7th street ",
      "cairo",
      "cairo",
      "1233999",
      "122",
      randomUserName,
      "alki",
      "alki"
    );
    console.log(user);
    //go to open a new account
    await account.goToOpencreateAccount();
    //open a two account for a new one which has by defult one so will have 3 accounts
    await account.doCreateAnAccount();
    const messageCreateFirst = page.getByRole("heading", {
      name: "Account Opened!",
    });
    //expect to have a message error confirm open the account
    await expect(messageCreateFirst).toBeVisible();
    await account.goToOpencreateAccount();
    await account.doCreateAnAccount();
    const messageForCreateSecond = page.getByRole("heading", {
      name: "Account Opened!",
    });
    // go to overivew accounts to check the total balance
    await AccountOverview.doGoToOverview();
    const total = page.locator("(//b[@class='ng-binding'])[1]");
    //assert to have total plance "3,500,000"
    await expect(total).toHaveText("3,500,000");

        //add screen shot 
    await testInfo.attach("create 3 accounts",{
      body:await page.screenshot(),
      contentType:"image/png"
  });});
  test("transfer from -to the same account A blocking message will appear", async ({
    page,
  },testInfo) => {
    //create anew objects from classes
    const LoginObj = new LoginPage(page);
    const transferObj = new Transfer(page);
    //go to login with valid cred
    await LoginObj.goToLoginPage();
    await LoginObj.doLogin({ userName: "ali", password: "akram" });
    await transferObj.doTransfer();
    //go to transfer page
    await expect(page).toHaveURL("http://localhost:8080/parabank/transfer.htm");
    //transfer from -to the same account
    await transferObj.doTransfer();
    //assert to not be able to transfer from-to the same account
    const locator = page.getByRole("heading", { name: "Transfer Complete!" });
    await expect(locator).toHaveText(
      "You can not transfer the amount to same account."
    );
    await testInfo.attach("transfer from -to the same account",{
      body:await page.screenshot(),
      contentType:"image/png"
  });
  });
  test("Try to transfer amount more than the balance in account", async ({
    page,
  },testInfo) => {
    //create a new object from classes

    const LoginObj = new LoginPage(page);
    const transferObj = new Transfer(page);
    //go to login with valid cred
    await LoginObj.goToLoginPage();
    await LoginObj.doLogin({ userName: "ali", password: "akram" });
    await transferObj.doTransfertToOtherAccount();
    //go to transfer page
    await expect(page).toHaveURL("http://localhost:8080/parabank/transfer.htm");
    //transfer from -to the same account
    await transferObj.doTransfertToOtherAccount();
    //assert to not be able to transfer from-to the same account
    const locator = page.getByRole("heading", { name: "Transfer Complete!" });
    await expect(locator).toHaveText(
      "You can not transfer more than thebalance in your account."
    );
    await testInfo.attach("Try to transfer amount more than the balance ",{
      body:await page.screenshot(),
      contentType:"image/png"
  });
  });

  //for this test case will consideration total amount less than 5000
  test("Request a loan from the selected account with downpayment less than loan amount ", async ({
    page,
  },testInfo) => {
    //create a new object from classes
    const LoginObj = new LoginPage(page);
    const LoanObj = new Loan(page);
    const SignUpObj = new SignUp(page);

    await SignUpObj.doGoToRegsiter();
    //genrate random name for username

    let randomUserName = (Math.random() + 1).toString(36).substring(7);
    console.log("random user name ==", randomUserName);
    const user = await SignUpObj.doSignUp(
      "mo",
      "ahmed",
      "7th street ",
      "cairo",
      "cairo",
      "1233999",
      "122",
      randomUserName,
      "alki",
      "alki"
    );
    console.log(user);

    //do request to loan
    await LoanObj.doRequestLoan({
      amountForLoan: "10",
      amountForDownpayment: "5",
    });
    //asert to have a Congratulations message
    const messageForApprove = page.getByText("Congratulations, your loan");
    await expect(messageForApprove).toContainText("Congratulations");

    await testInfo.attach("Request a loan ",{
      body:await page.screenshot(),
      contentType:"image/png"
  });
  });
  test("Request a loan from the selected account with downpayment more than loan amount ", async ({
    page,
  },testInfo) => {
    //create a new object from classes
    const LoginObj = new LoginPage(page);
    const LoanObj = new Loan(page);

    await LoginObj.goToLoginPage();
    //login
    await LoginObj.doLogin({ userName: "ali", password: "akram" });
    //do request to loan
    await LoanObj.doRequestLoan({
      amountForLoan: "5000",
      amountForDownpayment: "10000",
    });
    //asert to have a Congratulations message
    const messageForApprove = page.getByText(
      "You do not have sufficient funds for the given down payment."
    );
    await expect(messageForApprove).toContainText(
      "You do not have sufficient funds for the given down payment."
    );

    await testInfo.attach("Request a loan  downpayment more than loan amount",{
      body:await page.screenshot(),
      contentType:"image/png"
  });
  });
  test("Pay the bill less than total amount", async ({ page },testInfo) => {
    //create a new object from the classes
    const SignUpObj = new SignUp(page);
    const Bill = new BillPayment(page);
    const AccountOverview = new AccountsOverview(page);

    // go to sign up  page
    await SignUpObj.doGoToRegsiter();
    //genrate random name for username

    let randomUserName = (Math.random() + 1).toString(36).substring(7);
    console.log("random user name ==", randomUserName);
    const user = await SignUpObj.doSignUp(
      "mo",
      "ahmed",
      "7th street ",
      "cairo",
      "cairo",
      "1233999",
      "122",
      randomUserName,
      "alki",
      "alki"
    );
    console.log(user);
    await Bill.goToBill();
    await Bill.doBillPayment({
      payName: randomUserName,
      amountForBill: "515.50",
    });
    const message = page.getByText("Bill Payment Complete");
    await expect(message).toContainText(" Bill Payment Complete");
    await AccountOverview.doGoToOverview();
    const total = page.locator("(//b[@class='ng-binding'])[1]");
    //assert to have total plance "0" amount
    await expect(total).toHaveText("$0.00");


    await testInfo.attach("Pay the bill less than total amount",{
      body:await page.screenshot(),
      contentType:"image/png"
  });
  });
  test("Pay the bill more than total amount", async ({ page },testInfo) => {
    //create a new object from the classes
    const SignUpObj = new SignUp(page);
    const Bill = new BillPayment(page);
    const AccountOverview = new AccountsOverview(page);

    // go to sign up  page
    await SignUpObj.doGoToRegsiter();
    //genrate random name for username
    let randomUserName = (Math.random() + 1).toString(36).substring(7);
    console.log("random user name ==", randomUserName);
    const user = await SignUpObj.doSignUp(
      "mo",
      "ahmed",
      "7th street ",
      "cairo",
      "cairo",
      "1233999",
      "122",
      randomUserName,
      "alki",
      "alki"
    );
    console.log(user);
    await Bill.goToBill();
    await Bill.doBillPayment({
      payName: randomUserName,
      amountForBill: "515.50",
    });
    await AccountOverview.doGoToOverview();
    const total = page.locator("(//b[@class='ng-binding'])[1]");
    //assert to have total plance "0" amount
    await expect(total).toHaveText("$0.00");
    await Bill.goToBill();
    await Bill.doBillPayment({
      payName: randomUserName,
      amountForBill: "515.50",
    });
    const message = page.getByText("Bill Payment Complete");
    await expect(message).toContainText(
      "You can not pay more than balance in your account"
    );

    await testInfo.attach("Pay the bill more than total amount",{
      body:await page.screenshot(),
      contentType:"image/png"
  });
  });
});

