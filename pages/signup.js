exports.SignUp = class SignUp {
  constructor(page) {
    this.page = page;
    this.firstName = page.locator('input[name="customer.firstName"]');
    this.lastName = page.locator('input[name="customer.lastName"]');
    this.address = page.locator('[id="customer\\.address\\.street"]');
    this.city = page.locator('[id="customer\\.address\\.city"]');
    this.state = page.locator('[id="customer\\.address\\.state"]');
    this.zipCode = page.locator('[id="customer\\.address\\.zipCode"]');
    this.ssn = page.locator('[id="customer\\.ssn"]');
    this.userName = page.locator('[id="customer\\.username"]');
    this.password = page.locator('[id="customer\\.password"]');
    this.confirmPassword = page.locator("#repeatedPassword");
    this.registerBtn = page.getByRole("button", { name: "Register" });
  }

  async doGoToRegsiter() {
    await this.page.goto("http://localhost:8080/parabank/register.htm");
  }
  async doSignUp( firstname, lastName, address, city, state, zipCode, ssn,userName, password,
    confirmPassword
  ) {
    await this.firstName.fill(firstname);
    await this.lastName.fill(lastName);
    await this.address.fill(address);
    await this.city.fill(city);
    await this.state.fill(state);
    await this.zipCode.fill(zipCode);
    await this.ssn.fill(ssn);
    await this.userName.fill(userName);
    await this.password.fill(password);
    await this.confirmPassword.fill(confirmPassword);
    await this.registerBtn.click();

    console.log(userName, password);

    return {userName, password};
  }


};
