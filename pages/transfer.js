exports.Transfer=class Transfer{
    constructor(page){
        //add locters

        this.page=page;
       this.goToTransfer = page.getByRole('link',{name:'Transfer Funds'});
       this.addAmount=page.locator('#amount');
       this.transferBtn=page.getByRole('button',{name:'Transfer'});
       this.transfertToOtherAccountBtn= page.locator('#toAccountId');

    
    }
  //methods for made actions

async doTransfer(amount='10'){
 await this.goToTransfer.click();
 await this.addAmount.fill(amount);
 await this.transferBtn.click();
}
async doTransfertToOtherAccount(amount="35000000"){
    await this.goToTransfer.click();
    await this.addAmount.fill(amount);
    await this.transfertToOtherAccountBtn.selectOption('16119');
    await this.transferBtn.click();

}
    }
