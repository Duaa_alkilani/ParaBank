exports.BillPayment = class BillPayment {
  constructor(page) {
    //add locters
    this.page = page;
    this.billPayment=page.getByRole('link',{name:'Bill Pay'})
    this.payeeName = page.locator('input[name="payee\\.name"]');
    this.street = page.locator('input[name="payee\\.address\\.street"]');
    this.city = page.locator('input[name="payee\\.address\\.city"]');
    this.state = page.locator('input[name="payee\\.address\\.state"]');
    this.zipCode = page.locator('input[name="payee\\.address\\.zipCode"]');
    this.phone = page.locator('input[name="payee\\.phoneNumber"]');
    this.account = page.locator('input[name="payee\\.accountNumber"]');
    this.verifyAccount = page.locator('input[name="verifyAccount"]');
    this.amount= page.locator('input[name="amount"]');
    this.sendPaymentBtn = page.getByRole("button", { name: "Send Payment" });}

    //methods for made actions
    async goToBill(){
        await this.page.goto('http://localhost:8080/parabank/billpay.htm')
    }
    async doBillPayment(data={payName,amountForBill}){
     await this.payeeName.fill(data.payName)
     await  this.street.fill("7th street")
     await this.city.fill('cairo')
     await this.state.fill('cairo')
     await this.zipCode.fill('1234')
     await this.phone.fill('123456789')
     await this.account.fill('1234')
     await this.verifyAccount.fill('1234')
     await this.amount.fill(data.amountForBill)
     await this.sendPaymentBtn.click();
    }
  }

