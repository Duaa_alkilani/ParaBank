exports.Loan=class Loan{
    constructor(page){
 //add locters

        this.page=page;
        this.requestloan = page.getByRole('link',{name:'Request Loan'});
        this.loanAmount=page.locator('#amount');
        this.downPayment=page.locator('#downPayment')
        this.applyNowBtn=page.getByRole('button',{name:'Apply Now'})
        this.selectAccount= page.locator('#fromAccountId');
    }
      //methods for made actions

   async doRequestLoan(amount={amountForLoan,amountForDownpayment}){
    await this.requestloan.click();
    await this.loanAmount.fill(amount.amountForLoan)
    await this.downPayment.fill(amount.amountForDownpayment)
    // await this.selectAccount.selectOption('16119');
    await this.applyNowBtn.click()

   }

}