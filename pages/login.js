exports.LoginPage=class LoginPage {
  constructor(page) {
    //add locters
    this.page = page;
    this.username_text = page.locator('input[name="username"]');
    this.password_text = page.locator('input[name="password"]');
    this.login_btn = page.getByRole("button", { name: "Log In" });
  }
  //methods for made actions
  async doLogin(user={userName,password}) {
    await this.username_text.fill(user.userName);
    await this.password_text.fill(user.password);
    await this.login_btn.click();
  }
  async goToLoginPage(){
    await this.page.goto('http://localhost:8080/parabank/index.htm')
   
  }
}
